package main

import (
	"database/sql"
	_ "embed"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/eknkc/basex"
	"gopkg.in/yaml.v2"
)

var pgConn *sql.DB

type appConfigStruct struct {
	Database struct {
		Host         string `yaml:"host"`
		Port         int    `yaml:"port"`
		DatabaseName string `yaml:"database"`
		Password     string `yaml:"password"`
		User         string `yaml:"username"`
	} `yaml:"database"`
	UniqueCodeTemplate    string `yaml:"uniqueCodeTemplate"`
	UniqueCodeCharacters  string `yaml:"uniqueCodeCharacters"`
	UniqueCodeDigits      int    `yaml:"uniqueCodeDigits"`
	UniqueCodesToGenerate int    `yaml:"uniqueCodesToGenerate"`
	ShouldAddNewOnly      *bool  `yaml:"shouldAddNewNotAddUpTo"`
	MaxDuplicateError     int    `yaml:"maxDuplicateError"`
	CodeTableName         string `yaml:"codeTableName"`
	CodeTableColumn       string `yaml:"codeTableColumn"`
	ShouldWriteOutputFile bool   `yaml:"shouldWriteOutputFile"`
	OutputFileName        string `yaml:"outputFileName"`
}

var appConfig appConfigStruct

//go:embed config.yaml.example
var stdExampleConfig string

func readAppConfig() {
	configFileName := flag.String("config", "config.yaml", "The config file to read configuration")
	outputFileName := flag.String("out", "", "The output file to write to")
	codeTemplate := flag.String("codeTemplate", "", "Go Template syntax for unique code to be generated")
	codeCharacters := flag.String("codeCharacters", "", "Character space for the unique code. MUST be unique")
	codeDigits := flag.Int("codeDigit", 0, "Length of the unique code")
	codeCount := flag.Int("codeCount", 0, "Count of unique codes to generate")
	var shouldAddNew *bool
	flag.BoolFunc("addNew", "add new codeCount unique codes instead of generating up to codeCount", func(x string) error {
		v := true
		shouldAddNew = &v
		return nil
	})
	tableName := flag.String("tableName", "", "Table name to write unique code into.")
	tableColumn := flag.String("tableColumn", "", "Column to write unique code into")
	help := flag.Bool("help", false, "display this help. Note that all parameters, when defined, will override the values in the config file.")
	flag.Parse()
	if *help {
		flag.PrintDefaults()
		os.Exit(0)
	}
	if configFileName == nil || len(*configFileName) == 0 {
		n := "config.yaml"
		configFileName = &n
	}
	file, err := os.Open(*configFileName)
	if err != nil {
		if os.IsNotExist(err) {
			log.Println("Config file does not exist")
			err = os.WriteFile(*configFileName, []byte(stdExampleConfig), os.FileMode(0640))
			if err == nil {
				log.Fatalln("Wrote config file \"" + *configFileName + "\".")
			}
		}
		log.Fatalln("Failed to read configuration file")
	}
	yamlDecoder := yaml.NewDecoder(file)
	err = yamlDecoder.Decode(&appConfig)
	if err != nil {
		os.WriteFile("config.yaml.example", []byte(stdExampleConfig), os.FileMode(0640))
		log.Fatalln("Failed to decode configuration file. Reference format has been written to config.yaml.example")
	}
	if outputFileName != nil && len(*outputFileName) > 0 {
		appConfig.OutputFileName = *outputFileName
	}
	if codeTemplate != nil && len(*codeTemplate) > 0 {
		appConfig.UniqueCodeTemplate = *codeTemplate
	}
	if codeCharacters != nil && len(*codeCharacters) > 0 {
		appConfig.UniqueCodeCharacters = *codeCharacters
	}
	if codeDigits != nil && *codeDigits > 0 {
		appConfig.UniqueCodeDigits = *codeDigits
	}
	if codeCount != nil && *codeCount > 0 {
		appConfig.UniqueCodesToGenerate = *codeCount
	}
	if shouldAddNew != nil {
		appConfig.ShouldAddNewOnly = shouldAddNew
	}
	if tableName != nil && len(*tableName) > 0 {
		appConfig.CodeTableName = *tableName
	}
	if tableColumn != nil && len(*tableColumn) > 0 {
		appConfig.CodeTableColumn = *tableColumn
	}
	if strings.Contains(appConfig.CodeTableName, ";") ||
		strings.Contains(appConfig.CodeTableName, " ") ||
		strings.Contains(appConfig.CodeTableColumn, ";") ||
		strings.Contains(appConfig.CodeTableColumn, " ") {
		log.Fatalln("Illegal characters in config for tableName or tableColumn, refusing to run possible SQL injection")
	}
}

func connectToDatabase() {
	psqlConnStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		appConfig.Database.Host,
		appConfig.Database.Port,
		appConfig.Database.User,
		appConfig.Database.Password,
		appConfig.Database.DatabaseName)
	var err error
	pgConn, err = sql.Open("postgres", psqlConnStr)
	if err != nil {
		log.Fatalln("Failed to connect to database")
	}
}

var encoder *basex.Encoding

func makeEncoder() {
	// func encodeX (inTxt string) string {
	var err error
	encoder, err = basex.NewEncoding(appConfig.UniqueCodeCharacters)
	if err != nil {
		log.Fatalln("failed to create encoder")
	}
}
