module uniquecode-generator

go 1.16

require (
	github.com/eknkc/basex v1.0.0
	github.com/lib/pq v1.9.0
	gopkg.in/yaml.v2 v2.4.0
)
