package main

import (
	"crypto/rand"
	"fmt"
	"log"
	"math"
	"os"
	"strings"
)

var uniqueCodeBytes int
var outputFile *os.File
var uniqueCodesToGenerate int
var insertionQuery string

func calcUnicodeNeededBytes() {
	uniqueCodeCharsLength := len(appConfig.UniqueCodeCharacters)
	totalCombination := math.Pow(float64(appConfig.UniqueCodeDigits), float64(uniqueCodeCharsLength))
	uniqueCodeBytes = int(math.Ceil(math.Log(totalCombination) / math.Log(float64(2))))
}

func initUniqueCodeGeneration() {
	calcUnicodeNeededBytes()
	countQuery := fmt.Sprintf("select count(%s) from %s", appConfig.CodeTableColumn, appConfig.CodeTableName)
	rows := pgConn.QueryRow(countQuery)
	var currentCountOfUniqueCodesInDatabase int
	err := rows.Scan(&currentCountOfUniqueCodesInDatabase)
	if err != nil {
		log.Fatalf("initCountQuery: Failed scan countQuery: %s", err)
	}
	log.Printf("Current count of %s rows unique code in database: %d", appConfig.CodeTableName, currentCountOfUniqueCodesInDatabase)
	uniqueCodesToGenerate = appConfig.UniqueCodesToGenerate
	if appConfig.ShouldAddNewOnly != nil && !*appConfig.ShouldAddNewOnly {
		if uniqueCodesToGenerate <= currentCountOfUniqueCodesInDatabase {
			log.Fatalln("Database has enough unique code")
		}
		uniqueCodesToGenerate = uniqueCodesToGenerate - currentCountOfUniqueCodesInDatabase
	}
	if appConfig.ShouldWriteOutputFile {
		outputFile, err = os.OpenFile(appConfig.OutputFileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatalf("Failed to open output file %s for writing", appConfig.OutputFileName)
		}
		log.Printf("Starting to generate %d unique codes into %s.%s", uniqueCodesToGenerate, appConfig.CodeTableName, appConfig.CodeTableColumn)
		log.Printf("with writing / appending generated code into %s", appConfig.OutputFileName)
	} else {
		log.Printf("Starting to generate %d unique codes into %s.%s", uniqueCodesToGenerate, appConfig.CodeTableName, appConfig.CodeTableColumn)
	}
	insertionQuery = fmt.Sprintf("insert into %s (%s) values ($1);", appConfig.CodeTableName, appConfig.CodeTableColumn)
	generateUniqueCodes()
	log.Printf("Successfully generated %d unique codes.", uniqueCodesToGenerate)
}

func generateUniqueCodes() {
	var uniqueCode string
	var dupErrs int
	var remainingUniqueCodes = uniqueCodesToGenerate
	for {
		if dupErrs > appConfig.MaxDuplicateError {
			log.Fatalf(fmt.Sprintf("Duplicate errors exceeded %d, exiting", appConfig.MaxDuplicateError))
		}
		uniqueCode = generateUniqueCode()
		_, err := pgConn.Exec(insertionQuery, uniqueCode)
		if err == nil {
			remainingUniqueCodes--
			if appConfig.ShouldWriteOutputFile {
				_, writeToFileErr := outputFile.Write([]byte(uniqueCode + "\n"))
				if writeToFileErr != nil {
					log.Fatalf("Failed to write into the output file: %s", writeToFileErr)
				}
			}
		} else {
			if !(strings.Contains(err.Error(), "unique") || strings.Contains(err.Error(), "duplicate")) {
				log.Fatalf("Unexpected error: %s", err)
			}
			dupErrs++
		}
		if remainingUniqueCodes <= 0 {
			break
		}
	}
}

func generateUniqueCode() string {
	randomBytes := make([]byte, uniqueCodeBytes)
	rand.Read(randomBytes)
	encodedRand := encoder.Encode(randomBytes)
	return fmt.Sprintf(appConfig.UniqueCodeTemplate, encodedRand[0:appConfig.UniqueCodeDigits])
}
